package com.sample;

import com.sample.controller.PriceController;
import com.sample.model.dto.ProductItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PriceCalculatorIntegrationTest {
    private MockMvc mockMvc;

    @Autowired
    private PriceController priceController;

    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.priceController).build();
    }

    @Test
    public void getPage_ShouldReturnPricePage() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("price"))
                .andExpect(model().attribute("productList", hasSize(2)))
                .andExpect(model().attribute("productItem", notNullValue()));

    }

    @Test
    public void getPriceListPage_ShouldReturnPricePage() throws Exception {
        ProductItem productItem = new ProductItem();
        productItem.setProductId(1L);
        productItem.setItems(60);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/")
                .flashAttr("productItem", productItem))
                .andExpect(status().isOk())
                .andExpect(view().name("price"))
                .andExpect(model().attribute("priceList", hasEntry(1, 11.375)))
                .andExpect(model().attribute("priceList", hasEntry(20, 175.00)))
                .andExpect(model().attribute("finalPrice", equalTo( 472.50)));
    }
}

