package com.sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.controller.PriceController;
import com.sample.model.Product;
import com.sample.model.dto.ProductItem;
import com.sample.repository.ProductRepository;
import com.sample.service.PriceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PriceController.class)
public class PriceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private PriceService priceService;

    private ProductItem productItem;

    private Product penguinears;
    private Map<Integer, Double> itemsPriceMap;
    private static final double finalPrice = 175.00;

    @Before
    public void setUp() {
        itemsPriceMap = new HashMap<>();
        itemsPriceMap.put(1, 175.00);
        when(priceService.getPrices(anyLong())).thenReturn(itemsPriceMap);
        when(priceService.getPrice(ArgumentMatchers.any(ProductItem.class))).thenReturn(finalPrice);

        penguinears = new Product();
        penguinears.setId(1L);
        penguinears.setName("Penguinears");
        penguinears.setBoxPrice(BigDecimal.valueOf(175.00));
        penguinears.setItemsPerBox(20);

        List<Product> productList = new ArrayList<>();
        productList.add(penguinears);

        Mockito.when(productRepository.findAll())
                .thenReturn(productList);

        productItem = new ProductItem();
        productItem.setItems(1);
        productItem.setProductId(1L);
    }

    @Test
    public void getPage_ShouldReturnPricePage() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("price"))
                .andExpect(model().attribute("productList", contains(penguinears)));

    }

    @Test
    public void getPriceListPage_ShouldReturnPricePage() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/")
                .flashAttr("productItem", productItem))
                .andExpect(status().isOk())
                .andExpect(view().name("price"))
                .andExpect(model().attribute("priceList", hasEntry(1, itemsPriceMap.get(1))))
                .andExpect(model().attribute("finalPrice", equalTo(finalPrice)));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
