package com.sample;

import com.sample.model.Product;
import com.sample.service.PriceCalculatorService;
import com.sample.service.impl.PriceCalculatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)
public class PriceCalculatorServiceTest {
    private PriceCalculatorService priceCalculatorService;

    private Product product;

    @Before
    public void setUp(){
        priceCalculatorService = new PriceCalculatorServiceImpl();
        product = new Product();
        product.setItemsPerBox(20);
        product.setBoxPrice(BigDecimal.valueOf(175.00));
    }

    @Test
    public void getSingleItemPrice_ShouldReturnCorrectPrice(){
        Double price = priceCalculatorService.calculateTotalPrice(1, product);
        Assert.assertNotEquals(price,null);
        Assert.assertEquals(price,Double.valueOf(11.375));
    }

    @Test
    public void getPriceWithValidProductSingleBox_ShouldReturnPrice(){
        Double price =  priceCalculatorService.calculateTotalPrice(product.getItemsPerBox(), product);
        Assert.assertNotEquals(price,null);
        Assert.assertEquals(price,Double.valueOf(175.00));
    }

    @Test
    public void getPriceWithValidProductThreeBox_ShouldReturnPrice(){
        Double price =  priceCalculatorService.calculateTotalPrice(product.getItemsPerBox() * 3, product);
        Assert.assertNotEquals(price,null);
        Assert.assertEquals(price,Double.valueOf(472.50));
    }

    @Test
    public void getPriceWithValidProductTwoBoxNineteenItem_ShouldReturnPrice(){
        Double price =  priceCalculatorService.calculateTotalPrice(59, product);
        Assert.assertNotEquals(price,null);
        Assert.assertEquals(price,Double.valueOf(566.125));
    }

    @Test
    public void getPriceWithValidProductThreeBoxNineteenItem_ShouldReturnPrice(){
        Double price =  priceCalculatorService.calculateTotalPrice(79, product);
        Assert.assertNotEquals(price,null);
        Assert.assertEquals(price,Double.valueOf(667.0125));
    }
}
