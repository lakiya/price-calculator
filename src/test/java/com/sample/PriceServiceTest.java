package com.sample;

import com.sample.model.Product;
import com.sample.model.dto.ProductItem;
import com.sample.repository.ProductRepository;
import com.sample.service.PriceCalculatorService;
import com.sample.service.PriceService;
import com.sample.service.impl.PriceServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceTest {
    @Mock
    private ProductRepository productRepository;

    @Mock
    private PriceCalculatorService priceCalculatorService;

    private PriceService priceService;

    private ProductItem validProductItem;

    private Product penguinears;

    @Before
    public void setUp(){
        penguinears = new Product();
        penguinears.setId(1L);
        penguinears.setName("Penguinears");
        penguinears.setBoxPrice(BigDecimal.valueOf(175.00));
        penguinears.setItemsPerBox(20);

        Mockito.when(productRepository.findById(1L))
                .thenReturn(Optional.of(penguinears));

        Mockito.when(priceCalculatorService.calculateTotalPrice(any(Integer.class), any(Product.class)))
                .thenReturn(any(Double.class));

        priceService = new PriceServiceImpl(productRepository, priceCalculatorService);

        validProductItem = new ProductItem();
        validProductItem.setProductId(1L);
    }

    @Test
    public void getPricesWithValidProduct_ShouldReturnPriceMap(){
        Map<Integer, Double> productItemPriceMap = priceService.getPrices(1L);
        Assert.assertNotEquals(productItemPriceMap,null);
        Assert.assertEquals(productItemPriceMap.size(),50);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getPricesWithInvalidProduct_ShouldReturnEntityNotFoundException(){
        priceService.getPrices(4L);
    }

    @Test
    public void getPriceWithValidProductSingleItem_ShouldReturnPrice(){
        validProductItem.setItems(1);
        Double price = priceService.getPrice(validProductItem);
        Assert.assertNotEquals(price,null);
    }


    @Test(expected = EntityNotFoundException.class)
    public void getPriceWithInvalidProduct_ShouldReturnEntityNotFoundException(){
        ProductItem validProductItem = new ProductItem();
        validProductItem.setProductId(4L);
        validProductItem.setItems(1);
        priceService.getPrice(validProductItem);
    }
}
