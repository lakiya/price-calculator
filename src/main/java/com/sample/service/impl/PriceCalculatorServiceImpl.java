package com.sample.service.impl;

import com.sample.model.Product;
import com.sample.service.PriceCalculatorService;
import org.springframework.stereotype.Service;

@Service
public class PriceCalculatorServiceImpl implements PriceCalculatorService {

    @Override
    public Double calculateTotalPrice(Integer itemCount, Product product) {
        int itemsPerBox = product.getItemsPerBox();
        int boxes = itemCount / itemsPerBox;
        int items = itemCount % itemsPerBox;
        double boxPrice = product.getBoxPrice().doubleValue();

        //Apply 10% discount for more than 3 box purchase
        if(boxes >= 3){
            boxPrice = boxPrice - (boxPrice * 0.1);
        }
        double itemPrice = boxPrice / itemsPerBox;

        double totalBoxesPrice = boxes * boxPrice;
        double itemsPrice = items * itemPrice;

        //Add 30% for item purchases
        double totalItemsPrice = itemsPrice + (itemsPrice * 0.3);
        return totalBoxesPrice + totalItemsPrice;
    }
}
