package com.sample.service.impl;

import com.sample.model.Product;
import com.sample.model.dto.ProductItem;
import com.sample.repository.ProductRepository;
import com.sample.service.PriceCalculatorService;
import com.sample.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class PriceServiceImpl implements PriceService {

    private final ProductRepository productRepository;
    private final PriceCalculatorService priceCalculatorService;

    @Autowired
    public PriceServiceImpl(ProductRepository productRepository, PriceCalculatorService priceCalculatorService) {
        this.productRepository = productRepository;
        this.priceCalculatorService = priceCalculatorService;
    }

    @Override
    public Map<Integer, Double> getPrices(Long productId) {
        Optional<Product> optProduct = this.productRepository.findById(productId);

        if(!optProduct.isPresent()){
            throw new EntityNotFoundException();
        }

        Product product = optProduct.get();
        Map<Integer, Double> itemsPriceMap = new HashMap<>();

        for (int i = 1; i <= 50; i++) {
            double total = this.priceCalculatorService.calculateTotalPrice(i, product);
            itemsPriceMap.put(i, total);
        }
        return itemsPriceMap;
    }

    @Override
    public Double getPrice(ProductItem productItem) {
        Optional<Product> optProduct = this.productRepository.findById(productItem.getProductId());

        if(!optProduct.isPresent()){
            throw new EntityNotFoundException();
        }

        Product product = optProduct.get();
        return this.priceCalculatorService.calculateTotalPrice(productItem.getItems(), product);
    }
}
