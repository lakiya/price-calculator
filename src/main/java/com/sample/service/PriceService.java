package com.sample.service;

import com.sample.model.dto.ProductItem;

import java.util.Map;

public interface PriceService {
    Map<Integer, Double> getPrices(Long productId);

    Double getPrice(ProductItem productItem);
}
