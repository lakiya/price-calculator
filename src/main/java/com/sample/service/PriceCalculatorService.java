package com.sample.service;

import com.sample.model.Product;

public interface PriceCalculatorService {
    Double calculateTotalPrice(Integer itemCount, Product product);
}
