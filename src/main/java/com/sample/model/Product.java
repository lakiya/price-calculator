package com.sample.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class Product {
    @Id
    private Long id;

    private String name;

    private BigDecimal boxPrice;

    private Integer itemsPerBox;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBoxPrice(BigDecimal boxPrice) {
        this.boxPrice = boxPrice;
    }

    public BigDecimal getBoxPrice() {
        return boxPrice;
    }

    public void setItemsPerBox(Integer itemsPerBox) {
        this.itemsPerBox = itemsPerBox;
    }

    public Integer getItemsPerBox() {
        return itemsPerBox;
    }
}
