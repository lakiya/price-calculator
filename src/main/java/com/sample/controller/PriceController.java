package com.sample.controller;

import com.sample.model.dto.ProductItem;
import com.sample.repository.ProductRepository;
import com.sample.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class PriceController {

    private final ProductRepository productRepository;
    private final PriceService priceService;

    @Autowired
    public PriceController(ProductRepository productRepository, PriceService priceService) {
        this.productRepository = productRepository;
        this.priceService = priceService;
    }

    @GetMapping
    public String getPage(Model model){
        this.setGeneralAttributes(model);
        return "price";
    }

    @PostMapping
    public String getPriceListPage(@Valid @ModelAttribute("productItem") ProductItem productItem, Model model){
        model.addAttribute("productList", this.productRepository.findAll());
        model.addAttribute("priceList", this.priceService.getPrices(productItem.getProductId()));
        model.addAttribute("finalPrice", this.priceService.getPrice(productItem));
        model.addAttribute("productItem", productItem);
        return "price";
    }

    private void setGeneralAttributes(Model model){
        model.addAttribute("productList", this.productRepository.findAll());
        model.addAttribute("productItem", new ProductItem());
    }
}
